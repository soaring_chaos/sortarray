import java.util.*;
import java.util.Arrays;

public class Main {
    public static void main(String args[]) {
        Integer array[] = new Integer[7];
        for (int i = 0; i < array.length; i++) {
            array[i] = (int) (Math.random() * 100);
            System.out.print(array[i] + "  ");
        }
        System.out.println("\nСортировка по возрастанию \n");
        Arrays.sort(array);
        for (int i = 0; i < array.length; i++) {
            System.out.print(array[i] + " ");
        }

        System.out.println("\nСортировка по убыванию \n");
        Arrays.sort(array, Collections.reverseOrder());
        for (int i = 0; i < array.length; i++) {
            System.out.print(array[i] + " ");
        }

        }
    }