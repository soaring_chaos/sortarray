import java.util.Arrays;
import java.util.Comparator;

public class BigArrays {
    public static void main(String[] args) {
        Integer[][] matrix;
        matrix = new Integer[4][4];
        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix.length; j++) {
                matrix[i][j] = (int) (Math.random() * 100);
                System.out.print(matrix[i][j] + "\t");
            }
            System.out.println();
        }
        System.out.println("\nпо возрастанию\n");
        for (Integer[] i : matrix) Arrays.sort(i);// по возрастанию
        for(int i=0;i<matrix.length;i++)
            System.out.println(Arrays.toString(matrix[i]));
        System.out.println("\nпо убыванию\n");
        for(Integer[] j : matrix) // по убыванию
        {

            Arrays.sort(j, new Comparator <Integer>() {
                public int compare(Integer o1, Integer o2) {

                    if(o1<o2) return 1;
                    if(o1>o2) return -1;
                    return 0;
                }
            });
        }
        for(int i=0;i<matrix.length;i++)
            System.out.println(Arrays.toString(matrix[i]));
    }
}
